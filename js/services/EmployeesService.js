ecUserManager.factory('EmployeesService', function($resource, $log) {
    return $resource('http://10.1.0.38:8080/UserManagerAPI/api/v1/employees/employ/:id', {}, {
        getEmployees: {method: 'GET', isArray:true},
        getEmploy: {method: 'GET'},
        saveEmploy: {method: 'POST'},
        updateEmploy: {method: 'PUT'},
        removeEmploy: {method: "DELETE"}
    })
});