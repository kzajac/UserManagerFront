ecUserManager.factory('emailAddressListService', function($resource){
   return $resource('http://10.1.0.38:8080/UserManagerAPI/api/v1/mailinglists/emailaddresses', {}, {
       getAllEmailAddresses: {method: 'GET', isArray:true}
   })
});