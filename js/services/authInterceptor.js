ecUserManager.factory('authInterceptor', function ($rootScope, $q, $window, Session, $injector, $location) {
    return {
        request: function (config) {
            var authService = $injector.get("AuthService");
            config.headers = config.headers || {};
            if (authService.isAuthenticated()) {
                config.headers.Authorization = 'Bearer ' + $window.sessionStorage.token;
            } else {
                $location.path('/login');
            }
            return config;
        },
        responseError: function (response) {
            var authService = $injector.get("AuthService");
            if (response.status === 401) {
                console.log("401");
                Session.destroy();
            }
            return $q.reject(response);
        }
    };
});

ecUserManager.config(function ($httpProvider) {
    $httpProvider.interceptors.push('authInterceptor');
});