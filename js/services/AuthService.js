ecUserManager.factory('AuthService', function ($http, Session, $base64, $window) {
    var authService = {};

    authService.login = function (credentials) {
        var username = credentials.username.replace(/\W/g, '');
        var password = credentials.password.replace(/\W/g, '');
        return $http(
            {
                url: "http://10.1.0.38:8080/UserManagerAPI/api/v1/signin",
                method: 'POST',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded',
                    'Authorization': 'Basic ' + $base64.encode(username + ":" + password)
                },
                data: 'grant_type=client_credentials'
            }
        )
            .success(function (data) {
                Session.create(data);
                return Session.userId;
            })
    };

    authService.isAuthenticated = function () {
        if ($window.sessionStorage.userId) {
            if ((parseFloat($window.sessionStorage.iat) + (parseFloat($window.sessionStorage.expires_in/1000))) > (parseFloat(new Date().getTime()/1000))) {
                return true;
            } else {
                Session.destroy();
                return false;
            }
        } else {
            return false;
        }
    };

    return authService;
})