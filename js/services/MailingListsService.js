ecUserManager.factory('MailingListsService', function($resource, $log) {
   /* var resource = $resource('http://localhost:8080/UserManagerAPI/api/v1/mailinglists/mailinglist/:mailingListName',
        {mailingListName: "@mailingListName"},{isArray: true});
    return {
        getAllMailingLists: function() {
            return resource.query();
        },

        getSingleMailingList: function(name) {
            return resource.get({mailingListName: name});
        },

        saveMailingList: function(newMailingList) {
            return resource.save(angular.fromJson(newMailingList));
        },

        deleteMailingList: function(name) {
            return resource.delete({mailingListName: name});
        }
    }*/
    return $resource('http://10.1.0.38:8080/UserManagerAPI/api/v1/mailinglists/mailinglist/:mailingListName', {}, {
        getAllMailingLists: {method: 'GET', isArray:true},
        getSingleMailingList: {method: 'GET'},
        addEmailToList: {method: 'PUT'},
        removeEmailFromList: {method: 'PUT'},
        addNewMailingList: {method: 'POST'},
        removeMailingList: {method: "DELETE"}
    })
});