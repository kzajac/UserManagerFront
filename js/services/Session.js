ecUserManager.service('Session', function ($base64, $window) {
    this.create = function (data) {
        var toDecode = data.access_token.split('.')[1];
        var imax = toDecode.length;
        if ((imax % 4) === 2)
            toDecode += "=="
        else if ((imax % 4) === 1)
            toDecode += "="
        var claims = $base64.decode(toDecode);
        var JSONClaims = JSON.parse(claims)
        $window.sessionStorage.id = JSONClaims.jti;
        $window.sessionStorage.userId = JSONClaims.sub;
        $window.sessionStorage.iat = JSONClaims.iat;
        $window.sessionStorage.iss = JSONClaims.iss;
        $window.sessionStorage.token = data.access_token;
        $window.sessionStorage.expires_in = data.expires_in;
    };
    this.destroy = function () {
        $window.sessionStorage.id = null;
        $window.sessionStorage.userId = null;
        $window.sessionStorage.sub = null;
        $window.sessionStorage.iss = null;
        $window.sessionStorage.token = null;
        $window.sessionStorage.iat = null;
        $window.sessionStorage.expires_in  = null;
    };
})