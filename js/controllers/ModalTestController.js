ecUserManager.controller('ModalTestController', function ($uibModal, $scope, $http, $window, $log, $base64, $location) {

    $scope.open = function () {

        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: 'templates/modalContent.html',
            controller: 'ModalInstanceCtrl',
            size: 'sm'
        });

        modalInstance.result
            .then(function(user) {
                var username = user.username.replace(/\W/g, '');
                var password = user.password.replace(/\W/g, '');
                $http(
                    {
                        url: "http://localhost:8080/UserManagerAPI/api/v1/signin",
                        method: 'POST',
                        headers: {
                            'Content-Type': 'application/x-www-form-urlencoded',
                            'Authorization': 'Basic '+$base64.encode(username+":"+password)
                        },
                        data: 'grant_type=client_credentials'
                    }
                )
                    .success(function (data, status, headers, config) {
                        $window.sessionStorage.token = data.access_token;
                        $scope.message = 'Welcome';
                        $location.path('/');
                    })
                    .error(function (data, status, headers, config) {
                        // Erase the token if the user fails to log in
                        delete $window.sessionStorage.token;

                        // Handle login errors here
                        $scope.message = 'Error: Invalid user or password';
                    });
            })
            .catch(function() {
                console.log('anuluj');
            }
        );
    };

    if ($window.sessionStorage.token) {
        var toDecode = $window.sessionStorage.token.split('.')[1];
        var imax = toDecode.length;
        if ((imax %  4) === 2)
            toDecode+="=="
        else if ((imax % 4) ===1)
            toDecode+="="
        var claims = $base64.decode(toDecode);
        var JSONClaims = JSON.parse(claims)
        $scope.loggedUser = JSONClaims.iss;
    } else {
        $scope.loggedUser = "";
    }
});

// Please note that $uibModalInstance represents a modal window (instance) dependency.
// It is not the same as the $uibModal service used above.

ecUserManager.controller('ModalInstanceCtrl', function ($scope, $uibModalInstance) {

    $scope.ok = function (user) {;
        $uibModalInstance.close(user);
    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
})