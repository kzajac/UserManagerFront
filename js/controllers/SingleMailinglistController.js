'use strict';

ecUserManager.controller('SingleMailinglistController', function($scope, $route, $timeout, $log, MailingListsService, $resource, $routeParams) {
    if ($routeParams.mailinglistName) {
        $scope.message = {};
        MailingListsService.getSingleMailingList({mailingListName: $routeParams.mailinglistName}).$promise
            .then(function(response) {console.log("poszlo"); $scope.mailinglist = response; $scope.valid=true;})
            .catch(function(response) {
                console.log("chujnia");
                console.log(response);
                $scope.showError = true;
                $scope.doFade = false;
                $scope.message.class="alert alert-danger";
                $scope.message.type="error: ";
                $scope.message.content = response.data.error_description;
                $timeout(function(){
                    $scope.doFade = true;
                    window.location.href = "#/AllMailinglists";
                }, 2500);
            }
        )
    }

    $scope.addAddressToMailinglist = function(listName, addressToAdd) {
        var body = {
            action: "add",
            name: listName,
            address: addressToAdd
        };
        MailingListsService.addEmailToList({mailingListName: $routeParams.mailinglistName}, body).$promise
            .then(function(response) {
                $scope.showError = true;
                $scope.doFade = false;
                $scope.message.class="alert alert-success";
                $scope.message.type="success: ";
                $scope.message.content = response.OK;
                $timeout(function(){
                    $scope.doFade = true;
                    $route.reload();
                }, 2500);
            })
            .catch(function(response) {console.log(response);})
    };

    $scope.removeAddressFromMailinglist = function(listName, addressToRemove) {
        var body = {
            action: "remove",
            name: listName,
            address: addressToRemove
        };
        console.log(body);
        MailingListsService.removeEmailFromList({mailingListName: $routeParams.mailinglistName}, body).$promise
            .then(function(response) {
                $scope.showError = true;
                $scope.doFade = false;
                $scope.message.class="alert alert-success";
                $scope.message.type="success: ";
                $scope.message.content = response.OK;
                $timeout(function(){
                    $scope.doFade = true;
                    $route.reload();
                }, 2500);
            })
            .catch(function(response) {console.log(response);})
    };
})