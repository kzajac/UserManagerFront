'use strict';

ecUserManager.controller('AllMailinglistsController', function($scope, $route, $timeout, $uibModal, ngTableParams, MailingListsService, $log, $resource, $filter) {

    $scope.message = {};

    $scope.mailinglistTable = new ngTableParams( {
        page: 1,
        count: 10
    }, {
        getData: function($defer, params) {
            MailingListsService.getAllMailingLists().$promise
                .then(function(data) {
                    $scope.mailinglists = params.filter() ? $filter('filter')(data, params.filter()) : data;
                    params.total($scope.mailinglists.length);
                    $scope.mailinglists = $scope.mailinglists.slice((params.page() - 1) * params.count(), params.page() * params.count());
                    $scope.mailinglists = params.sorting() ? $filter('orderBy')($scope.mailinglists, params.orderBy()) : $scope.mailinglists;
                    $defer.resolve($scope.mailinglists);
                })
                .catch (function(response) {
                if (response.status !== 401) {
                    $scope.showError = true;
                    $scope.doFade = false;
                    $scope.message.class = "alert alert-danger";
                    $scope.message.type = "error: ";
                    $scope.message.content = response.data.error_description;
                    $timeout(function () {
                        $scope.doFade = true;
                    }, 2500);
                }
            })
        }
    })

    $scope.deleteMailinglist = function(mailinglist) {
        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: 'mailinglistDeleteConfirmationModal.html',
            controller: 'ModalInstanceCtrl2',
            size: 'sm'
        });

        modalInstance.result
            .then(function(selectedItem) {
                MailingListsService.removeMailingList({mailingListName: mailinglist}).$promise
                    .then(function(response) {
                        $scope.showError = true;
                        $scope.doFade = false;
                        $scope.message.class="alert alert-success";
                        $scope.message.type="success: ";
                        $scope.message.content = response.OK;
                        $timeout(function(){
                            $scope.doFade = true;
                            $route.reload();
                        }, 2500);
                    })
                    .catch(function(response) {console.log("chujnia");console.log(response);})
            })
            .catch(function() {
                $log.info('anuluj');
            }
        );
    };
})
