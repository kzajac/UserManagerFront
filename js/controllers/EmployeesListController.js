'use strict';

ecUserManager.controller('EmployeesListController', function($scope, $timeout, ngTableParams, EmployeesService, $log, $resource, $filter, $uibModal) {

    $scope.employeesTable = new ngTableParams( {
        page: 1,
        count: 10
    }, {
        getData: function($defer, params) {
            EmployeesService.getEmployees().$promise
                .then(function(data) {
                    $scope.employees = params.filter() ? $filter('filter')(data, params.filter()) : data;
                    params.total($scope.employees.length);
                    $scope.employees = $scope.employees.slice((params.page() - 1) * params.count(), params.page() * params.count());
                    $scope.employees = params.sorting() ? $filter('orderBy')($scope.employees, params.orderBy()) : $scope.employees;
                    $defer.resolve($scope.employees);
                })
                .catch (function(response) {console.log(response);})
        }
    }),

    $scope.deleteEmploy = function(id) {
        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: 'userDeleteConfirmationModal.html',
            controller: 'ModalInstanceCtrl',
            size: 'sm'
        });

        modalInstance.result
            .then(function(selectedItem) {
                EmployeesService.removeEmploy({id: id}).$promise
                    .then(function(data) {
                        $scope.employeesTable.reload();
                    })
                    .catch(function(response) {console.log("chujnia");console.log(response);})
            })
            .catch(function() {
                $log.info('anuluj');
            }
        );
    };
});

ecUserManager.controller('ModalInstanceCtrl', function ($scope, $uibModalInstance) {

    $scope.ok = function () {
        $uibModalInstance.close();
    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
})
