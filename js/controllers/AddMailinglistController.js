'use strict';

ecUserManager.controller('AddMailinglistController', function($scope, $timeout, emailAddressListService, MailingListsService) {

    $scope.message = {};
    $scope.maillistregex = /.*@econsulting\.com\.pl$/;

    emailAddressListService.getAllEmailAddresses().$promise
        .then(function(response) {
            $scope.allEmailAddresses = response;
        })
        .catch(function(response) {
            console.log(response);
            $scope.showError = true;
            $scope.doFade = false;
            $scope.message.class="alert alert-danger";
            $scope.message.type="error: ";
            $scope.message.content = response.data.error_description;
            $timeout(function(){
                $scope.doFade = true;
                window.location.href = "#/EmployeesList";
            }, 2500);
        })

    $scope.addMailinglist = function(MailingList) {
        MailingListsService.addNewMailingList(MailingList).$promise
            .then(function(response) {
                $scope.showError = true;
                $scope.doFade = false;
                $scope.message.class="alert alert-success";
                $scope.message.type="success: ";
                $scope.message.content = response.OK;
                $timeout(function(){
                    $scope.doFade = true;
                    window.location.href = "#/AllMailinglists";
                }, 2500);
            })
            .catch(function(response) {
                console.log(response);
                $scope.showError = true;
                $scope.doFade = false;
                $scope.message.class="alert alert-danger";
                $scope.message.type="error: ";
                $scope.message.content = response.data.error_description;
                $timeout(function(){
                    $scope.doFade = true;
                }, 2500);
            })
    }
});
