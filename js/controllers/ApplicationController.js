ecUserManager.controller('ApplicationController', function ($scope) {
    $scope.currentUser = null;
    $scope.setCurrentUser = function (user) {
        $scope.currentUser = user;
    };
})