'use strict';

ecUserManager.controller('AddEmployeeController', function($scope, $timeout, EmployeesService) {
    $scope.employ = {};
    $scope.message = {};

    $scope.addEmploy = function(employ) {
        console.log("qwe "+employ.name)
        EmployeesService.saveEmploy(employ).$promise
            .then(function (response) {
                $scope.showError = true;
                $scope.doFade = false;
                $scope.message.class="alert alert-success";
                $scope.message.type="success: ";
                $scope.message.content = response.OK;
                $timeout(function(){
                    $scope.doFade = true;
                }, 2500);
                $scope.employ = {};
            })
            .catch(function (response) {
                $scope.showError = true;
                $scope.doFade = false;
                $scope.message.class="alert alert-danger";
                $scope.message.type="error: ";
                $scope.message.content = response.data.error_description;
                $timeout(function(){
                    $scope.doFade = true;
                }, 2500);
            })
    }
})
