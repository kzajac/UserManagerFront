ecUserManager.controller('LoginController', function ($scope, AuthService, $rootScope, $location, $window) {

    $scope.credentials = {
        username: '',
        password: ''
    };
    $scope.login = function (credentials) {
        AuthService.login(credentials)
            .then(function (user) {
                $scope.setCurrentUser($window.sessionStorage.userId);
                if ($rootScope.returnToState) {
                    $location.path($rootScope.returnToState)
                } else {
                    $location.path('/');
                }
            })
            .catch(function () {
                $scope.authError = "login failed";
            })
    };
});