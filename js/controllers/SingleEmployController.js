'use strict';

ecUserManager.controller('SingleEmployController', function($scope, $route, $timeout, $resource, $routeParams, $log, $uibModal, EmployeesService) {
    if ($routeParams.employId) {
        $scope.message = {};
        EmployeesService.getEmploy({id: $routeParams.employId}).$promise
            .then(function(response) {$scope.employ = response; $scope.valid = true;})
            .catch(function(response) {
                console.log("chujnia");
                console.log(response);
                $scope.showError = true;
                $scope.doFade = false;
                $scope.message.class="alert alert-danger";
                $scope.message.type="error: ";
                $scope.message.content = response.data.error_description;
                $timeout(function(){
                    $scope.doFade = true;
                    window.location.href = "#/EmployeesList";
                }, 2500);
            })
    }

    $scope.removeEmploy = function() {
        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: 'userDeleteConfirmationModal.html',
            controller: 'ModalInstanceCtrl2',
            size: 'sm'
        });

        modalInstance.result
            .then(function(selectedItem) {
                EmployeesService.removeEmploy({id: $routeParams.employId}).$promise
                    .then(function(response) {console.log("udalo sie");})
                    .catch(function(response) {console.log(response);})
            })
            .catch(function() {
                $log.info('anuluj');
            }
        );
    };

    $scope.updateEmploy = function(updatedEmploy) {
        EmployeesService.updateEmploy({id: $routeParams.employId}, updatedEmploy).$promise
            .then(function(response) {
                $scope.showError = true;
                $scope.doFade = false;
                $scope.message.class="alert alert-success";
                $scope.message.type="success: ";
                $scope.message.content = response.OK;
                $timeout(function(){
                    $scope.doFade = true;
                    $route.reload();
                }, 2500);
            })
            .catch(function(response) {
                console.log(response);
                $scope.showError = true;
                $scope.doFade = false;
                $scope.message.class="alert alert-danger";
                $scope.message.type="error: ";
                $scope.message.content = response.data.error_description;
                $timeout(function(){
                    $scope.doFade = true;
                }, 2500);
            })
    };
})

ecUserManager.controller('ModalInstanceCtrl2', function ($scope, $uibModalInstance) {

    $scope.ok = function () {
        $uibModalInstance.close();
    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
})