'use strict';

var ecUserManager = angular.module('ecUserManager', ['angular-loading-bar', 'ngRoute', 'ngTable', 'ngResource', 'checklist-model',
    'ngAnimate', 'ui.bootstrap', 'base64'])
    .config(function ($routeProvider) {
        $routeProvider.when('/EmployeesList',
            {
                templateUrl: '/templates/EmployeesList.html',
                controller: 'EmployeesListController',
                authenticate: true
            });
        $routeProvider.when('/SingleEmploy/:employId',
            {
                templateUrl: '/templates/singleemploy.html',
                controller: 'SingleEmployController',
                authenticate: true
            });
        $routeProvider.when('/AddEmploy',
            {
                templateUrl: '/templates/AddEmploy.html',
                controller: 'AddEmployeeController',
                authenticate: true
            });
        $routeProvider.when('/AllMailinglists',
            {
                templateUrl: '/templates/allmailinglists.html',
                controller: 'AllMailinglistsController',
                authenticate: true
            });
        $routeProvider.when('/SingleMailinglist/:mailinglistName',
            {
                templateUrl: '/templates/singlemailinglist.html',
                controller: 'SingleMailinglistController',
                authenticate: true
            });
        $routeProvider.when('/AddMailinglist',
            {
                templateUrl: '/templates/addmailinglist.html',
                controller: 'AddMailinglistController',
                authenticate: true
            });
        $routeProvider.when('/login',
            {
                templateUrl: '/templates/login.html',
                controller: 'LoginController',
                authenticate: false
            });
        $routeProvider.otherwise(
            {
                redirectTo: '/',
                authenticate: false
            });
    })
    .run(function ($rootScope, $location, AuthService, $log) {
        $rootScope.$on('$routeChangeStart', function(event, toState, fromState) {
            if (!AuthService.isAuthenticated() && toState.authenticate) {
                $rootScope.returnToState = toState.originalPath;
                $location.path('/login');
            }
        })
    })